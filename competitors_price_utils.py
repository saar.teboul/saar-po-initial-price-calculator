from tqdm import tqdm
from matplotlib import pyplot as plt
import optimized_params
from scipy.stats import norm
import re
from typing import Union
import numpy as np
import pandas as pd
from scipy import stats
from dataclasses import dataclass
from typing import List
import os
import db_dtypes
from gcp_utils import get_bigquery_client, query_as_df


def build_competitive_landscape(
    origin: str, destination: str, cabin: str, is_direct: bool, start_issue_date: str, end_issue_date: str, departure_date_shifts: list
):
    """
    Builds a DataFrame containing competitive landscape data for flights between two airports,
    adjusting prices based on specific conditions for relevant carriers and directions.

    Args:
        origin (str): The origin airport code (e.g., "LHR").
        destination (str): The destination airport code (e.g., "DEL").
        cabin (str): The cabin class (e.g., "E").
        is_direct (bool): Whether to focus on direct flights or not.
        start_issue_date (str): The start date for issuing tickets (YYYY-MM-DD format).
        end_issue_date (str): The end date for issuing tickets (YYYY-MM-DD format).
        departure_date_shifts (list): A list of integers representing shifts in departure dates to consider (e.g., [-1, 1]).

    Returns:
        df_cl (pandas.DataFrame): A DataFrame containing competitor landscape data with corrected prices.

    Steps:

    1. **Read:** Reads competitive landscape data for the specified criteria using `read_competitive_landscape_level1`.
    2. **Filter:** Filters data by:
        * Carriers ("BA" and "AI")
        * Direction ("LHR-DEL" and "DEL-LHR")
        * One-way/round-trip ("is_outbound_commodity")
    3. **Correct Prices:** Applies adjustments to flight prices based on:
        * Carrier and direction combinations
        * Predefined values in `corrective_delta` (requires further explanation)
    4. **Transform:**
        * Removes flights from carrier "VS".
        * Removes one-way flights.
        * Transforms data for competitor analysis with `transform_competitor_landscape`:
            * Selects relevant columns for commodity information.
            * Shifts departure dates based on `departure_date_shifts`.
            * Groups and aggregates data by relevant fields.
    """
    # ------------------------ Reading part ------------------------
    df_cl = read_competitive_landscape_level1(start_issue_date, end_issue_date, origin, destination, cabin, is_direct)

    # ------------------------ Filtering part ------------------------
    lhr_del_outbound_E_BA = (
        (df_cl["origin_commodity"] == destination)
        & (df_cl["destination_commodity"] == origin)
        & (df_cl["cabin_commodity"] == cabin)
        & (df_cl["is_outbound_commodity"] == 1)
        & (df_cl["carrier"] == "BA")
    )

    lhr_del_inbound_E_BA = (
        (df_cl["origin_commodity"] == destination)
        & (df_cl["destination_commodity"] == origin)
        & (df_cl["cabin_commodity"] == cabin)
        & (df_cl["is_outbound_commodity"] == 0)
        & (df_cl["carrier"] == "BA")
    )

    del_lhr_outbound_E_BA = (
        (df_cl["origin_commodity"] == origin)
        & (df_cl["destination_commodity"] == destination)
        & (df_cl["cabin_commodity"] == cabin)
        & (df_cl["is_outbound_commodity"] == 1)
        & (df_cl["carrier"] == "BA")
    )

    del_lhr_inbound_E_BA = (
        (df_cl["origin_commodity"] == origin)
        & (df_cl["destination_commodity"] == destination)
        & (df_cl["cabin_commodity"] == cabin)
        & (df_cl["is_outbound_commodity"] == 0)
        & (df_cl["carrier"] == "BA")
    )

    lhr_del_outbound_E_AI = (
        (df_cl["origin_commodity"] == destination)
        & (df_cl["destination_commodity"] == origin)
        & (df_cl["cabin_commodity"] == cabin)
        & (df_cl["is_outbound_commodity"] == 1)
        & (df_cl["carrier"] == "AI")
    )

    lhr_del_inbound_E_AI = (
        (df_cl["origin_commodity"] == destination)
        & (df_cl["destination_commodity"] == origin)
        & (df_cl["cabin_commodity"] == cabin)
        & (df_cl["is_outbound_commodity"] == 0)
        & (df_cl["carrier"] == "AI")
    )

    del_lhr_outbound_E_AI = (
        (df_cl["origin_commodity"] == origin)
        & (df_cl["destination_commodity"] == destination)
        & (df_cl["cabin_commodity"] == cabin)
        & (df_cl["is_outbound_commodity"] == 1)
        & (df_cl["carrier"] == "AI")
    )

    del_lhr_inbound_E_AI = (
        (df_cl["origin_commodity"] == origin)
        & (df_cl["destination_commodity"] == destination)
        & (df_cl["cabin_commodity"] == cabin)
        & (df_cl["is_outbound_commodity"] == 0)
        & (df_cl["carrier"] == "AI")
    )

    # ------------------------ Correcting part ------------------------
    corrective_delta = np.where(
        lhr_del_outbound_E_BA,
        107,  # 214,
        np.where(
            del_lhr_inbound_E_BA,
            107,  # 214,
            np.where(
                lhr_del_outbound_E_AI,
                101,  # 202,
                np.where(
                    del_lhr_inbound_E_AI,
                    101,  # 202,
                    np.where(
                        lhr_del_inbound_E_BA,
                        107,  # 214,
                        np.where(
                            del_lhr_outbound_E_BA,
                            107,  # 214,
                            np.where(lhr_del_inbound_E_AI, 101, np.where(del_lhr_outbound_E_AI, 101, np.NaN)),  # 202,
                        ),
                    ),
                ),
            ),
        ),
    )  # 202,

    df_cl["alternative_flight_price"] = df_cl["price_net_min"] + corrective_delta

    # ------------------------ Transformation part ------------------------
    df_cl = df_cl[df_cl.carrier != "VS"]
    df_cl = df_cl[df_cl.is_one_way_commodity == 0]

    commodity_main_columns = [
        "observation_date",
        "departure_date",
        "origin_commodity",
        "destination_commodity",
        "cabin_commodity",
    ]

    commodity_secondary_columns = [
        "is_direct_commodity",
        "period_of_day",
        "carrier",
        "min_stay_commodity",
        "is_outbound_commodity",
        "is_one_way_commodity",
    ]

    df_cl = transform_competitor_landscape(
        df=df_cl,
        commodity_main_columns=commodity_main_columns,
        commodity_extra_columns=commodity_secondary_columns,
        departure_date_shifts=departure_date_shifts,
    )

    return df_cl


def read_competitive_landscape_level1(start_issue_date, end_issue_date, origin, destination, cabin, is_direct):
    """
    Reads competitive landscape data for flights between two airports
    from Google BigQuery for a specified date range, origin, destination, cabin class, and direction.

    Args:
        start_issue_date (str): The start date for issuing tickets (YYYY-MM-DD format).
        end_issue_date (str): The end date for issuing tickets (YYYY-MM-DD format).
        origin (str): The origin airport code (e.g., "LHR").
        destination (str): The destination airport code (e.g., "DEL").
        cabin (str): The cabin class (e.g., "E").
        is_direct (bool): Whether to focus on direct flights or not.

    Returns:
        df (pandas.DataFrame): A DataFrame containing competitive landscape data
        filtered by the specified parameters.

    Steps:

    1. **Construct BigQuery Query:**
        * Builds a SQL query string using f-strings to dynamically insert input parameters.
        * Selects all columns (`*`) from the `cbm_prices_analytics_fixed` table (or `cbm_prices_analytics` for alternative historical data).
        * Filters data based on:
            * Observation date range defined by `start_issue_date` and `end_issue_date`.
            * Origin, destination, and cabin matching the provided parameters.
            * Direct flight preference represented by `is_direct`.
    2. **Execute Query and Load Data:**
        * Creates a BigQuery client using `get_bigquery_client`.
        * Executes the constructed query and retrieves results as a pandas DataFrame using `query_as_df`.

    """
    query = f"""
        SELECT
          *
        FROM
            `fetcherr-virgin-prod.da_data_marts_us.cbm_prices_analytics_fixed`
            -- `fetcherr-virgin-prod.da_data_marts_us.cbm_prices_analytics`
        WHERE
          DATE(observation_date) >= DATE('{start_issue_date}') AND
          DATE(observation_date) < DATE('{end_issue_date}') AND
          origin_commodity = '{origin}' AND
          destination_commodity = '{destination}' AND
          cabin_commodity = '{cabin}' AND
          is_direct_commodity = {1 if is_direct else 0}
    """
    bq_client = get_bigquery_client()
    df = query_as_df(bq_client, query)
    return df


def transform_competitor_landscape(
    df: pd.DataFrame, commodity_main_columns: list, commodity_extra_columns: list, departure_date_shifts: list = None
):
    """
    Transforms a DataFrame containing competitive landscape data for flights
    into a structured format for further analysis.

    Args:
        df (pd.DataFrame): The DataFrame containing competitive landscape data.
        commodity_main_columns (list): A list of main commodity columns
        (e.g., origin, destination, cabin).
        commodity_extra_columns (list): A list of additional commodity columns
        (e.g., is_direct, min_stay, is_outbound, is_one_way).
        departure_date_shifts (list, optional): A list of days to shift departure dates for
        additional analysis (default: None).

    Returns:
        df (pd.DataFrame): The transformed DataFrame with restructured and enriched data.

    Steps:

    1. **Aggregate Minimum Alternative Flight Prices:**
        * Groups data by the specified main and extra commodity columns.
        * Calculates the minimum alternative flight price for each group.
        * Resets the index to obtain a flat DataFrame.

    2. **Transform Categorical Values:**
        * Converts the `min_stay_commodity` column to string format with "min_stay_" prefix.
        * Encodes boolean values in `is_direct_commodity`, `is_outbound_commodity`, and `is_one_way_commodity`
        into human-readable labels ("direct", "non-direct", etc.).

    3. **Pivot and Rename Columns:**
        * Pivots the DataFrame to have main commodity columns as rows and extra commodity columns as columns.
        * Values become the minimum alternative flight prices.
        * Flattens multi-level column names by removing empty components and joining remaining parts with "_".

    4. **Shift Departure Dates (Optional):**
        * If `departure_date_shifts` is provided, performs the following for each shift:
            * Creates a copy of the DataFrame.
            * Shifts the `departure_date` column by the specified number of days.
            * Merges the original and shifted DataFrames, adding "_shifted_by_" prefixes to columns of the shifted DataFrame.
            * Concatenates the merged DataFrames to the final result.
    """
    # ------------------------ CL level 2 ------------------------
    df = (
        df.groupby(commodity_main_columns + commodity_extra_columns)
        .agg({"alternative_flight_price": "min"})
        .reset_index()
    )

    df["min_stay_commodity"] = df["min_stay_commodity"].astype(str).radd("min_stay_")

    df["is_direct_commodity"] = np.where(
        df["is_direct_commodity"] == 0,
        "non-direct",
        np.where(df["is_direct_commodity"] == 1, "direct", df["is_direct_commodity"]),
    )

    df["is_outbound_commodity"] = np.where(
        df["is_outbound_commodity"] == 0,
        "inbound",
        np.where(df["is_outbound_commodity"] == 1, "outbound", df["is_outbound_commodity"]),
    )

    df["is_one_way_commodity"] = np.where(
        df["is_one_way_commodity"] == 0,
        "RT",
        np.where(df["is_one_way_commodity"] == 1, "OW", df["is_one_way_commodity"]),
    )

    # ------------------------ CL level 3 ------------------------
    df = df.pivot_table(
        index=commodity_main_columns, columns=commodity_extra_columns, values=["alternative_flight_price"]
    ).reset_index()
    df.columns = ["_".join(filter(None, col)).strip() for col in list(df.columns)]

    # ------------------------ CL level 4 ------------------------

    if departure_date_shifts is not None:
        temp_df = df.copy()
        alternative_flight_prices = [col for col in df.columns if col.startswith("alternative_flight_price")]

        pbar = tqdm(departure_date_shifts)

        for shift_days in pbar:
            df_shifted = temp_df.copy()
            df_shifted["departure_date"] = df_shifted["departure_date"] - pd.Timedelta(days=shift_days)

            sign = "+" if shift_days > 0 else ""
            pbar.set_description(f"Shifting by {sign}{shift_days} days")

            df = df.merge(
                df_shifted[commodity_main_columns + alternative_flight_prices],
                on=commodity_main_columns,
                how="left",
                suffixes=("", f"_shifted_by_{sign}{shift_days}"),
            )

    return df


def merge_squad1_and_alternatives(
    df_sq1: pd.DataFrame, df_alternatives: pd.DataFrame, merge_keys_columns: list, sq1_cols_1: list, sq1_cols_2: list
):
    """
    Merges a DataFrame containing SQUAD1 data with another DataFrame containing alternative flight data based on specific keys.

    Args:
        df_sq1 (pd.DataFrame): The DataFrame containing SQUAD1 data.
        df_alternatives (pd.DataFrame): The DataFrame containing alternative flight data.
        merge_keys_columns (list): A list of column names used to perform the merge (e.g., origin, destination, cabin).
        sq1_cols_1 (list): A list of column names from df_sq1 to keep after the merge.
        sq1_cols_2 (list): A list of column names from df_sq1 to merge with alternative flight data (e.g., target__cum_demand).

    Returns:
        df_merged (pd.DataFrame): The merged DataFrame containing both SQUAD1 and alternative flight data.

    Steps:

    1. **Prepare Data:**
        * Convert `flight__issue_date` in `df_alternatives` and `commodity__departure_date` to datetime type and then extract the date.
        * Filter `df_sq1` to include only columns needed for merging and target cumulated demand.
        * Assert that each group in `df_sq1_fil` defined by `merge_keys_columns` and `sq1_cols_1` has only one target cumulated demand value.

    2. **Merge DataFrames:**
        * Merge `df_sq1_fil` with `df_alternatives` on the specified `merge_keys_columns`.
        * Use a "left" join to keep all rows from `df_sq1_fil` even if corresponding entries are missing in `df_alternatives`.
    """
    df_alternatives["flight__issue_date"] = pd.to_datetime(df_alternatives["flight__issue_date"]).dt.date
    df_alternatives["commodity__departure_date"] = pd.to_datetime(df_alternatives["commodity__departure_date"]).dt.date

    df_sq1_fil = df_sq1[merge_keys_columns + sq1_cols_1 + sq1_cols_2]
    # assert df_sq1_fil.groupby(merge_keys_columns + sq1_cols_1)["target__cum_demand"].count().max() == 1
    #change col type:
    df_alternatives['flight__issue_date'] = pd.to_datetime(df_alternatives['flight__issue_date'])
    return pd.merge(left=df_sq1_fil, right=df_alternatives, on=merge_keys_columns, how="left")


class PriceOfAlternativeProperties:
    """
    This class represents characteristics and functionalities related to a specific price column
    containing pricing information for alternative flight properties.

    Attributes:

    * `alternative_col`: (str) The name of the price column containing alternative flight data.

    Methods:

    * `_is_contain_this_or_opposite(feature_name, opposite_name)`: (private)
        * Checks if the current price column name contains either the specified feature name or its opposite.
        * Used internally to determine relevant characteristics of the price data.

    * `is_evening` (bool):
        * True if the price column name indicates evening flights, False for morning flights.

    * `is_min_stay_7` (bool):
        * True if the price column name indicates a minimum stay of 7 days, False for 0 days.

    * `is_inbound` (bool):
        * True if the price column name indicates inbound flights, False for outbound flights.

    * `is_BA` (bool):
        * True if the price column name indicates prices for British Airways (BA), False for Air India (AI).
"""
    def _is_contain_this_or_opposite(self, feature_name, opposite_name):
        if feature_name in self.alternative_col:
            return True
        else:
            assert opposite_name in self.alternative_col
            return False

    def __init__(self, alternative_col: str):
        # say `price_netpyqyr_min_direct_podmorning_AI_ms0_INB_RT`
        self.alternative_col = alternative_col

        self.is_evening = self._is_contain_this_or_opposite("evening", "morning")
        self.is_min_stay_7 = self._is_contain_this_or_opposite("min_stay_7", "min_stay_0")
        self.is_inbound = self._is_contain_this_or_opposite("inbound", "outbound")
        self.is_BA = self._is_contain_this_or_opposite("BA", "AI")


def get_competitor_factor_params(trainable_params, df, alternative_col):
    """
    Extracts parameters for a competitor factor within a statistical model from trainable parameters, dataframe, and alternative price column information.

    Args:

    * trainable_params (dict): A dictionary containing trainable parameters for the model.
    * df (pd.DataFrame): The DataFrame containing relevant flight data.
    * alternative_col (str): The name of the price column representing the specific competitor factor.

    Returns:

    * competitor_factor_params (AlternativeElasticityParams): An instance of `AlternativeElasticityParams` containing the extracted parameters for the competitor factor.

    Steps:

    1. **Extract Feature Information:**
        * Create an instance of `PriceOfAlternativeProperties` with the provided `alternative_col`.
        * This object helps in identifying key characteristics of the competitor factor based on its column name (e.g., evening flights, minimum stay, carrier).

    2. **Get Trainable Parameter Subsets:**
        * Extract separate parameter subsets from `trainable_params` for mu, sigma, and weight corresponding to the specific competitor factor and its features.

    3. **Construct Competitor Factor Parameters:**
        * Initialize an instance of `AlternativeElasticityParams` with the extracted weight, mu, and sigma values.
    """
    alternative_flight_features = PriceOfAlternativeProperties(alternative_col)

    mu = get_mu(trainable_params, df, alternative_flight_features)
    sigma = get_sigma(trainable_params, df, alternative_flight_features, mu)
    weight = get_weight(trainable_params, df, alternative_flight_features)

    return AlternativeElasticityParams(weight=weight, mu=mu, sigma=sigma)


def get_mu(trainable_params: dict, df: pd.DataFrame, alt: PriceOfAlternativeProperties):
    """
    This function applies the offsets to the alternative price.

    The offset is calculated as a product of three levels of offsets
        1. The first level is the basic offset, which is the difference of EQUIVALENT PRICES
        2. The second level is the difference in EQUIVALENT PRICES between morning and evening flights
        3. The third level is the difference in EQUIVALENT PRICES between 0 and 7 min stays

    What is EQUIVALENT PRICE:
        Let's say a flight in VS includes a bag.
        Flight in BA costs 90$ but does not include a bag. The bag in BA costs 9$.

        So the “perceived EQUIVALENT PRICE” in the BA case would be 99$, the factor would be 1.1 (1.1*90=99)
        and we should expect highest elasticity for VS around 99$
    """
    alternative_carrier = "BA" if alt.is_BA else "AI"

    # This basic offset represents the difference of EQUIVALENT PRICES
    # between target Virgin commodity and alternative commodity,
    # taking into account only airline of the alternative and point-of-origin and direction of the target

    # This cascade basically does the following
    # Initialize the offset_od_level with NaNs
    offset_od_level = np.full(len(df), np.nan)
    offset_od_level = pd.Series(np.full(len(df), np.nan))

    is_DEL_outbound = np.logical_and(df["commodity__origin"] == "DEL", df["commodity__direction"] == "outbound")
    is_LHR_outbound = np.logical_and(df["commodity__origin"] == "LHR", df["commodity__direction"] == "outbound")
    is_DEL_inbound = np.logical_and(df["commodity__origin"] == "DEL", df["commodity__direction"] == "inbound")
    is_LHR_inbound = np.logical_and(df["commodity__origin"] == "LHR", df["commodity__direction"] == "inbound")

    offset_od_level[is_DEL_outbound] = trainable_params[f"mu_offset_{alternative_carrier}_DEL_outbound"]
    offset_od_level[is_LHR_outbound] = trainable_params[f"mu_offset_{alternative_carrier}_LHR_outbound"]
    offset_od_level[is_DEL_inbound] = trainable_params[f"mu_offset_{alternative_carrier}_DEL_inbound"]
    offset_od_level[is_LHR_inbound] = trainable_params[f"mu_offset_{alternative_carrier}_LHR_inbound"]

    # This offset is to be used on top of the first offset, and represents the difference in EQUIVALENT PRICES
    # between morning and evening flights, the assumption is that the difference is asymmetric, meaning if
    # morning is x% preferable than evening, then evening is x% less preferable than morning.
    offset_period_of_day_level = get_asymmetric_equivalence(
        # if the alternative flight is at the evening
        is_alternative_condition=alt.is_evening,
        commodity_values_column=df["commodity__period_of_day"],
        equivalent_commodity_value="evening",
        filling_value=trainable_params["mu_offset_we_morning_they_evening"],
    )

    # same as before, just instead of using morning/evening using here 0 vs 7 min stays.
    offset_min_stay_level = get_asymmetric_equivalence(
        # if the alternative flight min stay is 7
        is_alternative_condition=alt.is_min_stay_7,
        commodity_values_column=df["commodity__min_stay"],
        equivalent_commodity_value=7,
        filling_value=trainable_params["mu_offset_we_stay_0_they_stay_7"],
    )

    offset_departure_date_shift_level = 0

    # Updated regular expression to specifically target integers after "shifted_by_"
    shift_naming_pattern = r"shifted_by_([+-]?\d+)"

    if "shifted_by_" in alt.alternative_col:
        for param_name, value in trainable_params.items():
            if param_name.startswith("mu_offset_departure_date_shifted"):
                params_days_shift, offset = int(re.search(shift_naming_pattern, param_name).group(1)), value
                column_days_shift = int(re.search(shift_naming_pattern, alt.alternative_col).group(1))

                if params_days_shift == column_days_shift:
                    offset_departure_date_shift_level = offset

    price = df[alt.alternative_col]

    # combining all three offsets
    return (
            offset_od_level + offset_period_of_day_level + offset_min_stay_level + offset_departure_date_shift_level
    ) * price


def get_asymmetric_equivalence(
        is_alternative_condition: bool,
        commodity_values_column: pd.Series,
        equivalent_commodity_value: Union[str, int],
        filling_value: float,
):
    """
    Calculates an asymmetric equivalence mask based on a condition, a target commodity value, and a filling value.

    Args:

    * is_alternative_condition (bool): Indicates whether the function should treat the provided `commodity_values_column`
    as representing alternative options or not.
    * commodity_values_column (pd.Series): A Pandas Series containing the commodity values (e.g., flight durations, prices)
    for different options.
    * equivalent_commodity_value (Union[str, int]): The specific commodity value considered equivalent to the alternative options.
    * filling_value (float): The value to assign to non-equivalent options under the specified condition.

    Returns:

    * equivalence_mask (np.ndarray): A NumPy array containing 1.0 for entries satisfying the equivalence condition,
    and `filling_value` or its inverse (depending on the condition) for other entries.
    """
    if is_alternative_condition:
        return np.where(commodity_values_column == equivalent_commodity_value, 1.0, filling_value)
    else:
        reverse_value = 1 / filling_value
        return np.where(commodity_values_column != equivalent_commodity_value, 1.0, reverse_value)


def get_symmetric_equivalence(alt_condition: bool, commodity_vec_value, equivalent_commodity_value, value_if_different):
    """
    Calculates a symmetric equivalence mask based on a condition, a target commodity value, and a filling value.

    Args:
        alt_condition (bool): Indicates whether the provided `commodity_vec_value`
         represents alternative options or not.
        commodity_vec_value (pd.Series): A Pandas Series containing the commodity values
         (e.g., flight durations, prices) for different options.
        equivalent_commodity_value (Union[str, int]): The specific commodity value considered
         equivalent to all other options.
        value_if_different (float): The value to assign to options not deemed equivalent.

    Returns:
        equivalence_mask (np.ndarray): A NumPy array containing 1.0 for entries satisfying
         the equivalence condition, and `value_if_different` for other entries.
    """
    if alt_condition:
        return np.where(commodity_vec_value == equivalent_commodity_value, 1.0, value_if_different)
    else:
        return np.where(commodity_vec_value != equivalent_commodity_value, 1.0, value_if_different)


def get_sigma(trainable_params: dict, df: pd.DataFrame, alt: PriceOfAlternativeProperties, mu: pd.Series):
    """
    Calculates the sigma parameter within a competitor factor based on trainable parameters, dataframe, alternative price details, and mu value.

    **Args:**

    * trainable_params (dict): A dictionary containing relevant trainable parameters for calculating sigma.
    * df (pd.DataFrame): The DataFrame containing flight data with relevant features.
    * alt (PriceOfAlternativeProperties): An instance of `PriceOfAlternativeProperties` representing the specific competitor factor.
    * mu (pd.Series): A Pandas Series containing the mu parameter values for the competitor factor.

    **Returns:**

    * sigma (pd.Series): A Pandas Series containing the calculated sigma values for each data point.
    """
    offset_od_level = trainable_params["sigma_base_BA"] if alt.is_BA else trainable_params["sigma_base_AI"]

    offset_period_of_day_level = get_symmetric_equivalence(
        alt.is_evening,
        df["commodity__period_of_day"],
        equivalent_commodity_value="evening",
        value_if_different=trainable_params["sigma_different_period_of_day_multiplier"],
    )

    offset_min_stay_level = get_symmetric_equivalence(
        alt.is_min_stay_7,
        df["commodity__min_stay"],
        equivalent_commodity_value=7,
        value_if_different=trainable_params["sigma_different_min_stay_multiplier"],
    )

    return (offset_od_level + offset_period_of_day_level + offset_min_stay_level) * mu


def get_weight(trainable_params: dict, df: pd.DataFrame, alt: PriceOfAlternativeProperties):
    """
    Calculates the weight parameter within a competitor factor based on trainable parameters, dataframe, and alternative price details.

    **Args:**

    * trainable_params (dict): A dictionary containing relevant trainable parameters for calculating weight.
    * df (pd.DataFrame): The DataFrame containing flight data with relevant features.
    * alt (PriceOfAlternativeProperties): An instance of `PriceOfAlternativeProperties` representing the specific competitor factor.

    **Returns:**

    * weight (pd.Series): A Pandas Series containing the calculated weight values for each data point.
    """
    offset_base = trainable_params["weight_base_BA"] if alt.is_BA else trainable_params["weight_base_AI"]

    offset_period_of_day = get_symmetric_equivalence(
        alt.is_evening,
        df["commodity__period_of_day"],
        equivalent_commodity_value="evening",
        value_if_different=trainable_params["weight_different_period_of_day_multiplier"],
    )

    offset_min_stay = get_symmetric_equivalence(
        alt.is_min_stay_7,
        df["commodity__min_stay"],
        equivalent_commodity_value=7,
        value_if_different=trainable_params["weight_different_min_stay_multiplier"],
    )

    nan = np.where(df[alt.alternative_col].isna(), 0.0, 1.0)
    for_index = df[alt.alternative_col] * 0.0 + 1.1

    return (offset_base + offset_period_of_day + offset_min_stay) * nan * for_index


def bounds_to_constraints(bounds_list):
    """
    Converts a list of lower and upper bounds into a list of optimization constraints.

    **Args:**

    * bounds_list (list): A list of tuples, where each tuple represents the lower and upper bounds for a single optimization variable. Each bound can be `None` if no restriction is applied.

    **Returns:**

    * constraints (list): A list of dictionaries, where each dictionary represents a single optimization constraint with the following keys:
        * type (str): The type of constraint, which is "ineq" for inequality constraints in this case.
        * fun (callable): A callable that takes the optimization variable vector (represented as `x`) and optionally a bound value (represented as `lb` or `ub`) as arguments, and returns the inequality expression for the constraint.
    """
    constraints = []
    for i, (lower_bound, upper_bound) in enumerate(bounds_list):
        # Add lower bound constraint if it's not None
        if lower_bound is not None:
            constraints.append({"type": "ineq", "fun": lambda x, lb=lower_bound: x[i] - lb})

        # Add upper bound constraint if it's not None
        if upper_bound is not None:
            constraints.append({"type": "ineq", "fun": lambda x, ub=upper_bound: ub - x[i]})
    return constraints


def calculate_price_offset(
        our_flight_prices: pd.DataFrame,
        alternative_flights_prices: pd.DataFrame,
        alternative_flights_sigmas: pd.DataFrame,
        alternative_flights_weights: pd.DataFrame,
):
    """
    Calculates the price offset for each flight option in a DataFrame based on alternative flight prices, sigmas, weights, and a reference distribution.

    **Args:**

    * our_flight_prices (pd.DataFrame): A DataFrame containing our flight prices as numeric values, with each column representing a specific price point.
    * alternative_flights_prices (pd.DataFrame): A DataFrame containing alternative flight prices as numeric values, with each row representing a flight option and each column corresponding to the same price points as `our_flight_prices`.
    * alternative_flights_sigmas (pd.DataFrame): A DataFrame containing standard deviation values for the alternative flight prices, with each row representing a flight option and each column corresponding to the same price points as `our_flight_prices`.
    * alternative_flights_weights (pd.DataFrame): A DataFrame containing weights for each alternative flight option, with each row representing a flight option and each column corresponding to the same price points as `our_flight_prices`.

    **Returns:**

    * price_offset (pd.DataFrame): A DataFrame containing the calculated price offset for each flight option in `our_flight_prices`, with the same index and columns renamed to "[price_point]_offset".

    **Steps:**

    1. **Shape Validation:** The function performs several assertions to ensure all DataFrames have compatible shapes:
        * `our_flight_prices` and `alternative_flights_prices` should have the same number of rows.
        * `alternative_flights_sigmas` and `alternative_flights_weights` should have the same number of rows as `alternative_flights_prices`.

    2. **Handle Missing Values:**
        * Replace missing values (NaNs) in `alternative_flights_prices` with a predefined placeholder value (100 in this case).
        * Replace missing values in `alternative_flights_sigmas` with another predefined value (10 in this case).
        * Replace missing values in `alternative_flights_weights` with zero.

    3. **Normalize Weights:** Divide each weight in `alternative_flights_weights` by the sum of weights for its corresponding flight option (row) to obtain normalized weights.

    4. **Calculate Cumulative Distribution Function (CDF):**
        * Use the `norm.cdf` function from the `scipy.stats` module to calculate the CDF for each price point in `our_flight_prices` compared to the corresponding price points in `alternative_flights_prices` with their respective sigmas.

    5. **Apply Weights and Sum:**
        * Repeat the normalized weights across the price point dimension to match the shape of the CDF.
        * Multiply the CDF with the normalized weights for each flight option and price point.
        * Sum the weighted CDF values across all alternative flight prices for each price point in `our_flight_prices`.

    6. **Offset Adjustment:**
        * Scale the summed values to a desired range by multiplying by 2.0 and subtracting 1.0.

    7. **Return Output:**
        * Create a new DataFrame with the calculated price offsets as values, using the same index as `our_flight_prices` and columns renamed to "[price_point]_offset".
    """
    # We are working here in 3 dimensions:
    # 1 - sample (commodity)
    # 2 - price point in sample
    # 3 - alternative flight's price for the sample

    m = our_flight_prices.shape[1]
    n = alternative_flights_prices.shape[0]
    a = alternative_flights_weights.shape[1]

    assert our_flight_prices.shape[0] == n
    assert alternative_flights_sigmas.shape[0] == n
    assert alternative_flights_weights.shape[0] == n

    # finites_map is a boolean DataFrame indicating where alternative_prices is finite
    finites_map = alternative_flights_prices.notna()

    # Use DataFrame.where() to replace NaNs with specific values
    alternative_flights_prices = alternative_flights_prices.where(finites_map, 100)
    alternative_flights_sigmas = alternative_flights_sigmas.where(finites_map, 10)
    alternative_flights_weights = alternative_flights_weights.where(finites_map, 0.0)

    # Normalize weights
    weights_normalized = alternative_flights_weights.div(alternative_flights_weights.sum(axis=1), axis=0)

    # Calculate the CDF for each alternative price
    comparison_df = norm.cdf(
        our_flight_prices.to_numpy().reshape(n, m, 1),
        loc=alternative_flights_prices.values.reshape(n, 1, a),
        scale=alternative_flights_sigmas.values.reshape(n, 1, a),
    )

    assert comparison_df.shape == (n, m, a)
    weights_normalized = np.repeat(weights_normalized.to_numpy()[:, np.newaxis, :], m, axis=1)

    # Calculate the weighted offset
    offset = weights_normalized * comparison_df
    offset = offset.sum(axis=2)

    # Adjust the offset to the desired range
    offset = (offset * 2.0) - 1.0

    return pd.DataFrame(offset, index=our_flight_prices.index, columns=[f"{x}_offset" for x in our_flight_prices])


# def calculate_price_offset_for_params(row, df, price_column, return_params=False):
#     """
#     Calculates the price offset for each flight option in a DataFrame based on parameters from a
#     competitor factor dictionary, now dynamically selected based on 'commodity__cabin'.
#
#     Args:
#         row (pd.Series): A row from the DataFrame.
#         df (pd.DataFrame): A DataFrame containing flight data.
#         price_column (str): Name of the column containing our flight prices.
#         return_params (bool): Whether to return the calculated mu, sigma, and weight DataFrames.
#
#     Returns:
#         A DataFrame with calculated price offset, and optionally mu, sigma, and weight DataFrames.
#     """
#     cabin_type = row['commodity__cabin']
#     optimized_params_var_name = f"optimized_params_{cabin_type}"
#     optimized_params_dict = getattr(optimized_params, optimized_params_var_name, None)
#
#     if optimized_params_dict is None:
#         raise ValueError(f"Optimized params not found for cabin type: {cabin_type}")
#
#     alternative_cols = [x for x in df.columns if x.startswith("alternative_flight_price_")]
#
#     mus = pd.DataFrame()
#     sigmas = pd.DataFrame()
#     weights = pd.DataFrame()
#
#     for alternative_col in alternative_cols:
#         competitors_factor_params = get_competitor_factor_params(optimized_params_dict, df, alternative_col)
#
#         mus[alternative_col] = competitors_factor_params.mu
#         sigmas[alternative_col] = competitors_factor_params.sigma
#         weights[alternative_col] = competitors_factor_params.weight
#
#     offset = calculate_price_offset(
#         our_flight_prices=df[[price_column]],
#         alternative_flights_prices=mus,
#         alternative_flights_sigmas=sigmas,
#         alternative_flights_weights=weights,
#     )
#
#     if return_params:
#         return offset, mus, sigmas, weights
#     else:
#         return offset


def calculate_price_offset_for_params(params: dict, df: pd.DataFrame, price_column: str, return_params=False):
    """
    Calculates the price offset for each flight option in a DataFrame based on alternative flight prices, sigmas, weights, and a reference distribution, using parameters from a competitor factor dictionary.

    **Args:**

    * params (dict): A dictionary containing parameters for calculating the price offset for each competitor factor.
    * df (pd.DataFrame): A DataFrame containing flight data with alternative flight price columns and potentially other relevant features.
    * price_column (str): The name of the column in `df` containing our flight prices.
    * return_params (bool, optional): Whether to additionally return the calculated mu, sigma, and weight DataFrames for each competitor factor (defaults to False).

    **Returns:**

    * price_offset (pd.DataFrame): A DataFrame containing the calculated price offset for each flight option in `df`, with the same index and columns renamed to "[price_point]_offset".
    * mu (pd.DataFrame, optional): Only returned if `return_params` is True. A DataFrame containing the calculated mu values for each alternative flight option.
    * sigma (pd.DataFrame, optional): Only returned if `return_params` is True. A DataFrame containing the calculated sigma values for each alternative flight option.
    * weight (pd.DataFrame, optional): Only returned if `return_params` is True. A DataFrame containing the calculated weight values for each alternative flight option.
    """
    alternative_cols = [x for x in df.columns if x.startswith("alternative_flight_price_")]

    mus = pd.DataFrame()
    sigmas = pd.DataFrame()
    weights = pd.DataFrame()

    for alternative_col in alternative_cols:
        competitors_factor_params = get_competitor_factor_params(params, df, alternative_col)

        mus[alternative_col] = competitors_factor_params.mu
        sigmas[alternative_col] = competitors_factor_params.sigma
        weights[alternative_col] = competitors_factor_params.weight

    offset = calculate_price_offset(
        our_flight_prices=df[[price_column]],
        alternative_flights_prices=mus,
        alternative_flights_sigmas=sigmas,
        alternative_flights_weights=weights,
    )

    if return_params:
        return offset, mus, sigmas, weights
    else:
        return offset


def plot_offset_analysis(mus, sigmas, weights, offset, representative_price_col, dft, df_name: str):
    """
    Visualizes the distribution of mu, sigma, weight parameters, and the calculated price offset, along with correlations with demand for each commodity group.

    **Args:**

    * mus (pd.DataFrame): A DataFrame containing the calculated mu values for each alternative flight option.
    * sigmas (pd.DataFrame): A DataFrame containing the calculated sigma values for each alternative flight option.
    * weights (pd.DataFrame): A DataFrame containing the calculated weight values for each alternative flight option.
    * offset (pd.DataFrame): A DataFrame containing the calculated price offset for each flight option.
    * representative_price_col (str): The name of the column in `df` representing the primary alternative flight price to compare against.
    * dft (pd.DataFrame): A DataFrame containing flight data with relevant features, potentially derived from `df` used in `calculate_price_offset_for_params`.
    * df_name (str): Name of the dataset used to generate the mus, sigmas, weights, and offset DataFrames.

    **Returns:**

    * None

    **Visualizations:**

    1. **Overall Distribution:**
        * Three histograms are plotted side-by-side:
            * One for the distribution of all mu values across all alternative flight options.
            * One for the distribution of all sigma values across all alternative flight options.
            * One for the distribution of all weight values across all alternative flight options.
    2. **Commodity-Specific Analysis:**
        * Multiple subplots are generated, each representing a specific commodity group:
            * One subplot shows the distribution of the chosen `representative_price_col` values for the current commodity group.
            * Another subplot shows the distribution of the calculated price offset for the same commodity group.
            * A third subplot displays the correlation between the price offset and the target_cum_demand for the current commodity group with additional information displayed:
                * Correlation coefficient between offset and demand.
                * Percentage of data points with missing offset values.
                * Number of data points used for the correlation analysis.
    """
    _, axes = plt.subplots(ncols=3, figsize=(20, 5))

    axes[0].hist(mus.stack().reset_index(drop=True), bins=50)
    axes[0].set_title(f"mu ({df_name} dataset)")

    axes[1].hist(sigmas.stack().reset_index(drop=True), bins=50)
    axes[1].set_title(f"sigmas ({df_name} dataset)")

    axes[2].hist(weights.stack().reset_index(drop=True), bins=50)
    axes[2].set_title(f"weights ({df_name} dataset)")
    plt.show()

    print("Now, plots per each commodity")

    _, axes = plt.subplots(ncols=3, nrows=8, figsize=(20, 20))

    dft = dft.copy()
    dft["offset"] = offset
    idx = 0

    def plot_for_one_commodity(one_commodity):
        nonlocal idx
        axes[idx, 0].hist(one_commodity[representative_price_col].reset_index(drop=True), bins=50)
        axes[idx, 0].set_title(representative_price_col)

        axes[idx, 1].hist(one_commodity["offset"], bins=50)
        axes[idx, 1].set_title(
            f"offset {one_commodity['commodity__period_of_day'].values[0]}-"
            f"{one_commodity['commodity__direction'].values[0]}-"
            f"{one_commodity['commodity__min_stay'].values[0]}"
        )

        corr = one_commodity["target__cum_demand"].corr(one_commodity["offset"])
        correlations = one_commodity.groupby(one_commodity["offset"].round(1))["target__cum_demand"].mean()

        nans_percentage = np.isnan(one_commodity["offset"]).sum() / len(one_commodity["offset"])

        axes[idx, 2].plot(correlations)
        axes[idx, 2].set_title(
            f"Offset vs Demand correlation = {corr:.4f} | NANs: {nans_percentage:.2%} | {len(correlations)} points")
        idx += 1

    dft.groupby(
        [
            "commodity__origin",
            "commodity__destination",
            "commodity__cabin",
            "commodity__non_stop",
            "commodity__direction",
            "commodity__min_stay",
            "commodity__period_of_day",
        ]
    ).apply(plot_for_one_commodity)

    plt.tight_layout()
    plt.show()


@dataclass
class GeneralElasticityParams:
    elasticity_max: np.ndarray
    elasticity_0: np.ndarray
    price_0: np.ndarray


@dataclass
class AlternativeElasticityParams:
    weight: np.ndarray
    mu: np.ndarray
    sigma: np.ndarray


@dataclass
class ElasticityParams:
    alternatives: List[AlternativeElasticityParams]
    general: GeneralElasticityParams


def general_elasticity_curve(x_values, params: GeneralElasticityParams):
    """
    Calculates the elasticity curve for a generalized demand function with two elasticity points and a smooth transition between them.

    **Args:**

    * x_values (np.ndarray): An array of price values at which to calculate the elasticity.
    * params (GeneralElasticityParams): A namedtuple containing the following parameters for the general elasticity curve:
        * price_0 (float): The reference price at which the initial elasticity `elasticity_0` applies.
        * elasticity_0 (float): The initial elasticity (price sensitivity) at the reference price `price_0`.
        * elasticity_max (float or np.ndarray): The maximum elasticity (price sensitivity) reached at higher prices. If a single float is provided, it applies to all price points. Otherwise, an array of the same length as `x_values` can be used to specify elasticities for each price point individually.

    **Returns:**

    * y_values (np.ndarray): An array of elasticity values corresponding to the given price points in `x_values`.
    """
    c = np.arctanh(2 * params.elasticity_0 / params.elasticity_max - 1.0) - np.log(params.price_0)
    y_values = (
            params.elasticity_max[:, np.newaxis] * np.tanh(np.log(x_values) + c[:, np.newaxis]) / 2
            + params.elasticity_max[:, np.newaxis] / 2
    )
    return y_values


def alternative_elasticity_curve(x_values, params: AlternativeElasticityParams):
    """
    Calculates the alternative elasticity curve for a price distribution with multiple competitor offers, weighted by their corresponding importance.

    **Args:**

    * x_values (np.ndarray): An array of price values at which to calculate the elasticity.
    * params (AlternativeElasticityParams): A namedtuple containing the following parameters for the alternative elasticity curve:
        * mu (np.ndarray): An array of mean prices for each competitor offer (same length as `x_values`).
        * sigma (np.ndarray): An array of standard deviations for each competitor offer (same length as `x_values`).
        * weight (np.ndarray): An array of weights for each competitor offer (same length as `x_values`). These weights represent the relative importance of each competitor in calculating the overall elasticity.

    **Returns:**

    * y_values (np.ndarray): An array of combined elasticity values corresponding to the given price points in `x_values`.
    """
    y_values = (
            stats.norm.pdf(x_values[np.newaxis, :], loc=params.mu[:, np.newaxis], scale=params.sigma[:, np.newaxis])
            * params.weight[:, np.newaxis]
    )
    return y_values


def compute_e_values_with_alternatives(x_values, general_e_values, alternatives: List[AlternativeElasticityParams]):
    """
    Combines the general elasticity with the influence of multiple competitor offer distributions to form the overall price elasticity.

    **Args:**

    * x_values (np.ndarray): An array of price values at which to calculate the elasticity.
    * general_e_values (np.ndarray): An array of pre-calculated general elasticity values (based on a reference price and elasticity points) for the same price points as `x_values`.
    * alternatives (List[AlternativeElasticityParams]): A list of namedtuples containing parameters for multiple competitor offers, each represented by `AlternativeElasticityParams` with the following attributes:
        * mu (np.ndarray): An array of mean prices for each competitor offer (same length as `x_values`).
        * sigma (np.ndarray): An array of standard deviations for each competitor offer (same length as `x_values`).
        * weight (np.ndarray): An array of weights for each competitor offer (same length as `x_values`). These weights represent the relative importance of each competitor in calculating the overall elasticity.

    **Returns:**

    * e_values (np.ndarray): An array of combined elasticity values considering both the general elasticity and the influence of competitor offers, corresponding to the given price points in `x_values`.
    """
    e_values = general_e_values.copy()
    for alt_params in alternatives:
        e_values += alternative_elasticity_curve(x_values, alt_params)
    return e_values


def calculate_q_values_naked(p_values, e_values, delta_p):
    """
    Calculates the naked demand values (without competitor considerations) based on price points, price elasticity, and a price difference factor.

    **Args:**

    * p_values (np.ndarray): A 2D array of price points for different options and price increments (each row represents a single option's price curve).
    * e_values (np.ndarray): A 2D array of corresponding elasticity values for each price point in `p_values`.
    * delta_p (float): The price difference factor used to calculate the cumulative sum of the elasticity-to-price ratio.

    **Returns:**

    * q_values_naked (np.ndarray): A 2D array of calculated naked demand values for each price point in `p_values`.
    """
    e_div_p_cumsum = np.cumsum(e_values / p_values, axis=1) * delta_p
    q_values_naked = np.exp(-e_div_p_cumsum)
    return q_values_naked


def calculate_q_values(p_values, e_values_ge, e_values_al, p0, q0_ge):

    """
    Calculates the final demand values for each price point, considering both general elasticity, alternative offer influences, and reference demand information.

    **Args:**

    * p_values (np.ndarray): A 2D array of price points for different options and price increments (each row represents a single option's price curve).
    * e_values_ge (np.ndarray): A 2D array of corresponding general elasticity values for each price point in `p_values`.
    * e_values_al (np.ndarray): A 2D array of corresponding alternative offer elasticity values for each price point in `p_values`.
    * p0 (np.ndarray): An array of reference price points for each option (same length as the number of rows in `p_values`).
    * q0_ge (np.ndarray): An array of reference demand values for each option at their respective reference prices (same length as the number of rows in `p_values`).

    **Returns:**

    * q_values (np.ndarray): A 2D array of calculated final demand values for each price point in `p_values`.
    """
    delta_p = p_values[1] - p_values[0]
    q_values_naked_ge = calculate_q_values_naked(p_values, e_values_ge, delta_p)
    q_values_naked_al = calculate_q_values_naked(p_values, e_values_al, delta_p)

    initial_indices = np.abs(p_values - p0[:, np.newaxis]).argmin(axis=1)
    ge_factors = q0_ge / q_values_naked_ge[np.arange(q_values_naked_ge.shape[0]), initial_indices]

    q_values_0 = q_values_naked_ge[:, 0] * ge_factors
    al_factors = q_values_0 / q_values_naked_al[:, 0]
    q_values = q_values_naked_al * al_factors[:, np.newaxis]

    return q_values


def cap_value(row, folder_path):
    value = row['Demand_change_according_to_load_factor']
    file_name = f"Gradient_Desc_static_table_{row['commodity__cabin']}_{row['commodity__direction']}_{row['commodity__min_stay']}_{row['commodity__period_of_day']}.csv"
    file_path = os.path.join(folder_path, file_name)
    concluded_demand_table = pd.read_csv(file_path)
    lower_bound = concluded_demand_table[concluded_demand_table.target__cum_demand >= 0][
        'target__cum_demand'].min()
    upper_bound = concluded_demand_table[concluded_demand_table.target__cum_demand >= 0][
        'target__cum_demand'].max()
    if pd.isna(value):
        return np.nan
    if value < lower_bound:
        return lower_bound
    elif value > upper_bound:
        return upper_bound
    else:
        return value


def find_closest_bin(row, folder_path):
    """
    Finds the closest bin in the appropriate concluded_demand_table based on row parameters.

    Args:
    * row (pd.Series): A row from the DataFrame containing the required parameters.
    * folder_path (str): Path to the folder containing concluded_demand_table files.

    Returns:
    * float: The closest bin demand value.
    """

    if pd.isna(row['offset']):
        return np.nan

    # Construct the file name
    file_name = f"Gradient_Desc_static_table_{row['commodity__cabin']}_{row['commodity__direction']}_{row['commodity__min_stay']}_{row['commodity__period_of_day']}.csv"
    file_path = os.path.join(folder_path, file_name)

    # Load the appropriate table
    concluded_demand_table = pd.read_csv(file_path)

    # Calculate the absolute difference between offset and each mid_bin
    differences = abs(concluded_demand_table['mid_bin'] - row['offset'])

    # Find the index of the minimum difference
    min_index = differences.idxmin()

    # Return the corresponding target__cum_demand value
    return concluded_demand_table.loc[min_index, 'target__cum_demand']


# def find_closest_bin(offset, concluded_demand_table):
#     """
#     Caps a value within a specified range.
#
#     **Args:**
#
#     * value (float): The value to be capped.
#     * lower_bound (float): The lower bound of the allowed range.
#     * upper_bound (float): The upper bound of the allowed range.
#
#     **Returns:**
#
#     * float: The capped value within the specified range.
#     """
#     # Calculate the absolute difference between offset and each mid_bin
#     differences = abs(concluded_demand_table['mid_bin'] - offset)
#     if pd.isna(offset):
#         return np.nan
#     # Find the index of the minimum difference
#     min_index = differences.idxmin()
#     # Return the corresponding target__cum_demand value
#     return concluded_demand_table.loc[min_index, 'target__cum_demand']


def get_prices_mapped_to_offsets(
        from_price,
        to_price,
        steps,
        dft,
        optimized_params):
    """
    Creates a mapping between a range of price points and their corresponding calculated price offsets based on optimized model parameters and flight data.

    **Args:**

    * from_price (float): The starting price point for the generated price range.
    * to_price (float): The ending price point for the generated price range.
    * steps (int): The number of evenly spaced price points to generate within the range.
    * dft (pd.DataFrame): The DataFrame containing flight data used for calculating the price offsets. This DataFrame should include relevant features used by the price offset model.

    **Returns:**

    * prices_df (pd.DataFrame): A DataFrame containing the generated price points as separate columns in addition to the original data from `dft`.
    * mapped_offsets (pd.DataFrame): A DataFrame containing the calculated price offsets for each price point and corresponding rows in `dft`.
    """
    prices = np.linspace(from_price, to_price, steps)
    prices_str = [str(price) for price in prices]
    prices_df = pd.DataFrame(prices.reshape(1, -1).repeat(len(dft), axis=0),
                             index=dft.index,
                             columns=prices_str)
    dft = pd.concat([dft, prices_df], axis=1)
    mapped_offsets = pd.DataFrame()
    for col in prices_df.columns:
        mapped_offsets_temp = calculate_price_offset_for_params(params=optimized_params,
                                                                df=dft, price_column=col)
        mapped_offsets = pd.concat([mapped_offsets, mapped_offsets_temp], axis=1)
    return prices_df, mapped_offsets


# def find_nearest_offset(bin_value, concluded_rank_vs_demand_table):
#     """
#     Finds the nearest offset value from a given bin value within a table containing ranked target cumulative demand and corresponding mid-bin values.
#
#     **Args:**
#
#     * bin_value (float): The target bin value for which to find the nearest offset.
#     * concluded_rank_vs_demand_table (pd.DataFrame): A Pandas DataFrame containing the following columns:
#         * `target__cum_demand`: The ranked cumulative demand values.
#         * `mid_bin`: The mid-point values of each bin in the ranking.
#
#     **Returns:**
#
#     * float: The nearest offset value (mid-bin value) corresponding to the provided bin value, or `None` if no close match exists within a specified tolerance (default: no tolerance).
#     """
#     if pd.isna(bin_value):
#         return np.nan
#     # Calculate the absolute difference between bin_value and each mid_bin
#     differences = abs(concluded_rank_vs_demand_table['target__cum_demand'] - bin_value)
#
#     # Find the index with the minimum difference (ignoring tolerance)
#     min_index = differences.idxmin()
#
#     # Return the corresponding offset value if a match exists
#     return concluded_rank_vs_demand_table.loc[min_index, 'mid_bin']


def find_nearest_offset(row, folder_path):

    if pd.isna(row['Demand_change_according_to_load_factor_capped']):
        return np.nan

    # Construct the file name/path based on the row's information
    file_name = f"Gradient_Desc_static_table_{row['commodity__cabin']}_{row['commodity__direction']}_{row['commodity__min_stay']}_{row['commodity__period_of_day']}.csv"
    file_path = os.path.join(folder_path, file_name)

    # Load the appropriate table
    concluded_rank_vs_demand_table = pd.read_csv(file_path)

    # Calculate the absolute difference
    differences = abs(concluded_rank_vs_demand_table['target__cum_demand'] - row['Demand_change_according_to_load_factor_capped'])

    # Find the index with the minimum difference
    min_index = differences.idxmin()

    # Return the corresponding offset value
    return concluded_rank_vs_demand_table.loc[min_index, 'mid_bin']


def find_nearest_matching_offset_df(df, demand_column, offset_pattern):
    """
    Finds the "_offset" column with the closest matching value for each row in a dataframe.

    Args:
    df: The pandas DataFrame.
    demand_column: The name of the column containing the demand values.
    offset_pattern: The pattern of the offset columns (e.g., "_offset").

    Returns:
    A new DataFrame with a column named "nearest_offset_column" containing the closest match for each row.
    """
    # Get demand values and offset column names
    demand_values = df[demand_column]
    offset_columns = [col for col in df.columns if col.endswith(offset_pattern)]
    # Create a dictionary to store results
    nearest_matches = {}
    # Iterate through each row
    for index, demand_value in demand_values.items():
        # Extract offset values for the current row
        offset_values = [df.loc[index, col] for col in offset_columns]
        if pd.isna(demand_value):
            nearest_matches[index] = np.nan
        # Find the closest value and its corresponding index
        else:
            closest_index, closest_value = min(enumerate(offset_values), key=lambda t: abs(t[1] - demand_value))
        # Store the closest matching column name.
            nearest_matches[index] = offset_columns[closest_index]
    # Create a new DataFrame with the results
    nearest_df = pd.DataFrame({"nearest_offset_column_string": nearest_matches})
    # Combine the new column with the original DataFrame
    result_df = pd.concat([df, nearest_df], axis=1)
    result_df['price_adjusted_to_offset'] = result_df['nearest_offset_column_string'].map(
        lambda x: np.nan if pd.isna(x) else float(x.split(offset_pattern)[0]))
    return result_df

