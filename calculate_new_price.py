from utils import *
import seaborn as sns

import time
if __name__ == '__main__':
    #takes +- 6 minutes for a run
    print("⏳ Staring Calculation - This might take time (AVG ± 6Min)⏳")
    start_time = time.time()
    offset_ratio = determine_load_factor()
    # ratio is optimum / predicted
    ## if ratio > 1, than the optimum is better then predicted, so we need to increase demand
    ## if ratio < 1 - we need to reduce demand
    print(offset_ratio)
    result_df = apply_competitors_offset(offset_ratio)
    print("time elapsed: {:.2f}s".format(time.time() - start_time))
    sns.scatterplot(data=result_df, x='representative_price', y='price_adjusted_to_offset')
    plt.show()
    print(f"✅ Successfully Finished")
    result_df['load_factor_offset'] = offset_ratio
    # only use if we want to make sure price for min stay 0 is higher than min stay 7
    result_df = adjust_prices(df=result_df)
    # result_df.to_csv('Result_df_09_Sept.csv', index=False)
    print(1)

