from sklearn.linear_model import LinearRegression
from gcp_utils import *
from competitors_price_utils import *
import scipy.optimize
import numpy as np
from scipy.stats import norm
import pandas as pd
import matplotlib.pyplot as plt


# ================ This is the constants 'config' - probably needed to be extracted out ================
start_issue_date = '2023-01-01'
end_issue_date = '2024-04-07'
origin = 'DEL'
destination = 'LHR'
cabin = 'E'
is_direct = True
train_test_ratio = 0.70
flight_no = "VS303"
period_of_day = "evening"
departure_date = pd.to_datetime("2024-01-06").date()
last_dtd = 30
departure_date_shifts = [-1, 1]
# =======================================================================================================

def get_bigquery_client():
    project_id = "fetcherr-virgin-sandbox"
    bq_client = bigquery.Client(project=project_id)
    return bq_client


def read_competition_kpi_dev_period_of_day():
    """
    Reads competition KPI data for a specific flight route and cabin from BigQuery, focusing on the daily period of day breakdown.

    **Pre-requisites:**

    * The function assumes access to a BigQuery client configured with appropriate project and authentication details.
    * The `get_bigquery_client` function should be defined and return a valid BigQuery client object.
    * The `query_as_df` function should be defined and take a BigQuery client and a SQL query as arguments, returning the query results as a Pandas DataFrame.

    **Arguments:**

    * None

    **Returns:**

    * pd.DataFrame: A Pandas DataFrame containing competition KPI data for the specified route, cabin, and date range
    """
    assert is_direct == 1
    query = f'''
        SELECT
          snapshot_date,
          origin,
          destination,
          flight_no,
          period_of_day,
          cabin,
          departure_date,
          dtd,
          au,
          availability,
          capacity_fixed,
          load_factor,
          pax_running,
          pax_new_taxonomy_running,
          pax_other_od_old_tax_running
        FROM
          `fetcherr-virgin-sandbox.da_data_marts_us.competition_kpi_dev_period_of_day`
        WHERE
          DATE(snapshot_date) >= DATE('{start_issue_date}') AND
          DATE(snapshot_date) < DATE('{end_issue_date}') AND
          origin = '{origin}' AND
          destination = '{destination}' AND
          cabin = '{cabin}'
          AND DATE(departure_date) >= '2023-09-01'
    '''
    #TODO fetcherr-virgin-sandbox.da_data_marts_us.competition_kpi_dev_period_of_day table is not in prod env
    bq_client = get_bigquery_client()
    df = query_as_df(bq_client, query)
    return df


def generate_curves():
    """
    Generates various booking demand curves based on competition KPI data for a specific flight route and cabin in BigQuery.

    **Pre-requisites:**

    * Requires the `read_competition_kpi_dev_period_of_day` function to obtain competition KPI data from BigQuery.

    **Arguments:**

    * None

    **Returns:**

    * tuple: A tuple containing two DataFrames:
        * curves (pd.DataFrame): A DataFrame containing calculated booking demand curves for different metrics over time (demand to date).
            * Each curve represents the rolling median of the respective metric over a 7-day window, centered on the demand to date.
        * df_kpis (pd.DataFrame): The original competition KPI data DataFrame retrieved with `read_competition_kpi_dev_period_of_day`, including additional calculations:
            * `bookings_based_on_av`: Derived bookings based on authorized units minus available seats.
    """
    df_kpis = read_competition_kpi_dev_period_of_day()
    assert (df_kpis.pax_new_taxonomy_running + df_kpis.pax_other_od_old_tax_running == df_kpis.pax_running).all()
    df_kpis['bookings_based_on_av'] = df_kpis['au'] - df_kpis['availability']
    tt = df_kpis
    tt = tt[tt.dtd >= 0]
    curves = {}
    for col in ['pax_other_od_old_tax_running',
                'pax_new_taxonomy_running',
                'bookings_based_on_av',
                'pax_running']:
        #         curves[col] = tt.groupby('dtd')[col].mean().rolling(1, center=True).mean()
        curves[col] = tt[[col, 'dtd']].dropna(subset=[col]).groupby('dtd').median().rolling(7, center=True).mean()
    # Fill NaNs using linear regression
    for col in ['pax_other_od_old_tax_running',
                'pax_new_taxonomy_running',
                'bookings_based_on_av',
                'pax_running']:
        fill_nans_with_regression(curves[col], col)
    tickets_df = curves['pax_other_od_old_tax_running'].merge(curves['bookings_based_on_av'], on=['dtd'],
                                                              how='left').merge(curves['pax_new_taxonomy_running'],
                                                                                on=['dtd'], how='left')
    tickets_df['pct_tkts_other_raw'] = tickets_df['pax_other_od_old_tax_running'] / tickets_df['bookings_based_on_av']
    tickets_df['pct_tkts_ftchr_raw'] = tickets_df['pax_new_taxonomy_running'] / tickets_df['bookings_based_on_av']
    ## adjustment - sum of %, if its not 100 - i have offset in my data - the adjustment should compensate
    tickets_df['adjustment'] = tickets_df['pct_tkts_other_raw'] + tickets_df['pct_tkts_ftchr_raw']
    tickets_df['pct_tkts_other_adjusted'] = tickets_df['pct_tkts_other_raw'] / tickets_df['adjustment']
    tickets_df['pct_tkts_ftchr_adjusted'] = tickets_df['pct_tkts_ftchr_raw'] / tickets_df['adjustment']

    tickets_df['usual_drvd_bkg_other'] = tickets_df['pct_tkts_other_adjusted'] * tickets_df['bookings_based_on_av']
    tickets_df['usual_drvd_bkg_ftchr'] = tickets_df['pct_tkts_ftchr_adjusted'] * tickets_df['bookings_based_on_av']
    tickets_df.replace([np.inf, -np.inf, np.nan], 0, inplace=True)
    curves = tickets_df.copy()
    return curves, df_kpis


def get_flight_df(df, origin, destination, flight_no, departure_date):
    """
    Filters a DataFrame containing competition KPI data to a specific flight based on origin, destination, flight number, departure date, and period of day.

    **Args:**

    * df (pd.DataFrame): The DataFrame containing competition KPI data.
    * origin (str): The origin airport code for the desired flight.
    * destination (str): The destination airport code for the desired flight.
    * flight_no (str): The flight number for the desired flight.
    * departure_date (str): The departure date for the desired flight.
    * period_of_day (str, optional): The period of day for the desired flight (e.g., Morning, Afternoon, Evening). Defaults to all periods.

    **Returns:**

    * pd.DataFrame: A new DataFrame containing only the data for the specified flight and period of day (if provided).
    """
    answer_df = df.copy()
    answer_df = answer_df[answer_df.origin == origin]
    answer_df = answer_df[answer_df.destination == destination]
    answer_df = answer_df[answer_df.flight_no == flight_no]
    answer_df = answer_df[answer_df.period_of_day == period_of_day]
    answer_df = answer_df[answer_df.departure_date == departure_date]

    assert not answer_df.snapshot_date.duplicated().any()
    return answer_df


def get_flight_df_with_future_dtds(flight_df, max_dtd):
    """
    Expands a DataFrame containing competition KPI data for a specific flight by adding rows for future demand-to-date (DTD) values up to a specified maximum.

    **Args:**

    * flight_df (pd.DataFrame): The DataFrame containing competition KPI data for a specific flight, already filtered and prepared.
    * max_dtd (int): The maximum DTD value (number of days) to consider for the expanded DataFrame.

    **Returns:**
    * pd.DataFrame: A new DataFrame containing the original flight data with additional rows representing future DTDs up to the specified maximum.
    """
    complete_dtd = pd.DataFrame({'dtd': range(max_dtd)})
    answer_df = pd.merge(complete_dtd, flight_df, on='dtd', how='outer')
    answer_df = answer_df.sort_values(by='dtd')
    return answer_df


def get_decay_factor_by_half_life(x):
    """
    Calculates the decay factor based on a given half-life.

    **Args:**

    * half_life (float): The half-life value (number of units for the value to decay by half).

    **Returns:**

    * float: The decay factor representing the proportional decrease per unit.
    """

    return np.power(2, -1 / x)


def get_predicted_flight_df(curves,  # avg flights
                            flight_df,  # specific flight
                            hl_ftchr,  # half life fetcher - how many days to look back
                            hl_other,
                            last_dtd):  # last day that we can look over the data, if its 30,
    """
    Predicts booking trends for a specific flight based on average booking curves, historical data, and half-life decay factors.

    **Args:**

    * curves (pd.DataFrame): A DataFrame containing average booking demand curves for various metrics (e.g., passengers, bookings based on availability).
    * flight_df (pd.DataFrame): A DataFrame containing competition KPI data for the specific flight of interest.
    * hl_ftchr (int): The half-life value (number of days) for the decay of Fetcherr booking trends.
    * hl_other (int): The half-life value (number of days) for the decay of other booking trends (non-Fetcherr).
    * last_dtd (int): The last day with available data for the specific flight, representing the maximum DTD value for predictions.

    **Returns:**

    * pd.DataFrame: A new DataFrame containing the original flight data with additional columns for predicted booking trends:
        * `predicted_bookings_ftchr`: Predicted Fetcherr bookings for each DTD.
        * `predicted_bookings_other`: Predicted bookings from other sources (non-Fetcherr) for each DTD.
        * `predicted_bookings_total`: Predicted total bookings (sum of Fetcherr and other) for each DTD.
        * `predicted_bookings_ftchr_cumsum`: Cumulative predicted Fetcherr bookings over time.
        * `predicted_bookings_other_cumsum`: Cumulative predicted bookings from other sources over time.
        * `predicted_bookings_total_cumsum`: Cumulative predicted total bookings over time.
    """
    # then we will take all bokink for d-30 and predicts for dates 29 till 0,
    # project overall demand from avg with historical
    dft = flight_df.copy()
    actual_ticketings_ftchr = 'pax_new_taxonomy_running'
    actual_ticketings_other = 'pax_other_od_old_tax_running'

    # share of fetcherr booking
    fetcherr_share = dft[actual_ticketings_ftchr] / (dft[actual_ticketings_ftchr] + dft[actual_ticketings_other])
    other_share = dft[actual_ticketings_other] / (dft[actual_ticketings_ftchr] + dft[actual_ticketings_other])
    # adjustment as before
    ftchr_booking_raw = fetcherr_share * dft["bookings_based_on_av"]
    other_booking_raw = other_share * dft["bookings_based_on_av"]
    adjustment = (ftchr_booking_raw + other_booking_raw) / dft["bookings_based_on_av"]
    dft["ftchr_booking_adjusted"] = ftchr_booking_raw / adjustment
    dft["other_booking_adjusted"] = other_booking_raw / adjustment
    dft["ftchr_booking_adjusted_diff"] = dft["ftchr_booking_adjusted"].diff(-1)
    dft["other_booking_adjusted_diff"] = dft["other_booking_adjusted"].diff(-1)

    # orders pace not in fetcher
    usual_other_bookings = curves['usual_drvd_bkg_other'].diff(-1)
    dft["usual_other_bookings"] = dft["dtd"].map(usual_other_bookings)

    usual_ftchr_bookings = curves['usual_drvd_bkg_ftchr'].diff(-1)
    dft["usual_ftchr_bookings"] = dft["dtd"].map(usual_ftchr_bookings)
    assert dft.dtd.is_monotonic_increasing
    dft['dtn'] = dft.dtd - last_dtd

    dft['weight_ftchr'] = np.power(get_decay_factor_by_half_life(hl_ftchr), dft['dtn'])
    dft['weight_other'] = np.power(get_decay_factor_by_half_life(hl_other), dft['dtn'])

    dft['weight_ftchr'] = np.where(dft.dtd < last_dtd, 0, dft['weight_ftchr'])
    dft['weight_other'] = np.where(dft.dtd < last_dtd, 0, dft['weight_other'])
    dft["ftchr_booking_adjusted_diff"] = dft["ftchr_booking_adjusted_diff"].apply(pd.to_numeric, errors='coerce')
    dft["other_booking_adjusted_diff"] = dft["other_booking_adjusted_diff"].apply(pd.to_numeric, errors='coerce')
    dft = dft.merge(curves.reset_index()[['dtd', 'usual_drvd_bkg_other', 'usual_drvd_bkg_ftchr']], on='dtd',
                    how='inner')

    #     diff from yesterday * weight (HL) / normal situation

    ratio_ftchr = (dft["ftchr_booking_adjusted_diff"] * dft['weight_ftchr']).sum() / (
            dft['usual_ftchr_bookings'] * dft['weight_ftchr']).sum()
    ratio_other = (dft["other_booking_adjusted_diff"] * dft['weight_other']).sum() / (
            dft['usual_other_bookings'] * dft['weight_other']).sum()

    #     ratio_ftchr = (dft["ftchr_booking_adjusted"] * dft['weight_ftchr']).apply(pd.to_numeric, errors='coerce').sum() / (
    #                 dft['usual_drvd_bkg_ftchr'] * dft['weight_ftchr']).apply(pd.to_numeric, errors='coerce').sum()
    #     ratio_other = (dft["other_booking_adjusted"] * dft['weight_other']).apply(pd.to_numeric, errors='coerce').sum() / (
    #                 dft['usual_drvd_bkg_other'] * dft['weight_other']).apply(pd.to_numeric, errors='coerce').sum()

    dft['predicted_bookings_ftchr'] = np.where(dft.dtd > last_dtd, dft["ftchr_booking_adjusted_diff"],
                                               dft['usual_ftchr_bookings'] * ratio_ftchr)
    dft['predicted_bookings_other'] = np.where(dft.dtd > last_dtd, dft["other_booking_adjusted_diff"],
                                               dft['usual_other_bookings'] * ratio_other)

    # dft['predicted_bookings_ftchr'] = np.where(dft.dtd > last_dtd, dft["ftchr_booking_adjusted"],
    #                                            dft['usual_drvd_bkg_ftchr'] * ratio_ftchr)
    # dft['predicted_bookings_other'] = np.where(dft.dtd > last_dtd, dft["other_booking_adjusted"],
    #                                            dft['usual_drvd_bkg_other'] * ratio_other)

    dft['predicted_bookings_total'] = dft['predicted_bookings_other'] + dft['predicted_bookings_ftchr']
    dft['predicted_bookings_ftchr_cumsum'] = dft['predicted_bookings_ftchr'].iloc[::-1].cumsum()[::-1]
    dft['predicted_bookings_other_cumsum'] = dft['predicted_bookings_other'].iloc[::-1].cumsum()[::-1]
    dft['predicted_bookings_total_cumsum'] = dft['predicted_bookings_total'].iloc[::-1].cumsum()[::-1]

    return dft


def get_offset_ratio(flight_df):
    """
    Calculates the offset ratio for Fetcherr booking predictions based on predicted and actual booking data.

    **Args:**

    * flight_df (pd.DataFrame): The DataFrame containing predicted booking trends for a specific flight.

    **Returns:**

    * float: The offset ratio for Fetcherr booking predictions, representing the relative adjustment needed to align with the total available units (AUs).
    """
    total_aus = flight_df.loc[flight_df.dtd == last_dtd, 'au'].values[0]
    dft_dtd0 = flight_df[flight_df.dtd == 1]
    predicted_bookings_ftchr_sum_dtd0 = dft_dtd0['predicted_bookings_ftchr_cumsum'].values[0]
    predicted_bookings_other_sum_dtd0 = dft_dtd0['predicted_bookings_other_cumsum'].values[0]

    ftchr_booking_adjusted_sum = flight_df["ftchr_booking_adjusted_diff"].sum()
    ftchr_booking_adjusted_sum_last_dtd = flight_df.loc[
        flight_df.dtd >= last_dtd, "ftchr_booking_adjusted_diff"].sum()
    other_booking_adjusted_sum_last_dtd = flight_df.loc[
        flight_df.dtd >= last_dtd, "other_booking_adjusted_diff"].sum()
    predicted_bookings_total_sum_dtd0 = predicted_bookings_ftchr_sum_dtd0 + predicted_bookings_other_sum_dtd0
    predicted_bookings_offset = total_aus - predicted_bookings_total_sum_dtd0
    # predicted_remaining_bookings_fetcherr = predicted_bookings_ftchr_sum_dtd0 #- ftchr_booking_adjusted_sum
    predicted_remaining_bookings_fetcherr = predicted_bookings_ftchr_sum_dtd0 - ftchr_booking_adjusted_sum_last_dtd
    predicted_remaining_bookings_fetcherr = np.maximum(0.00001, predicted_remaining_bookings_fetcherr)
    predicted_remaining_optimal_bookings_fetcherr = predicted_remaining_bookings_fetcherr + predicted_bookings_offset
    offset_ratio = predicted_remaining_optimal_bookings_fetcherr / predicted_remaining_bookings_fetcherr  # check
    # "offset_ratio is < 1.0, need to reduce expected demand
    # "offset_ratio is > 1.0, need to stimulate expected demand
    return offset_ratio


def fill_nans_with_regression(df, col):
    """
    Fills missing values (NaNs) in a column of a DataFrame using linear regression.

    **Args:**

    * df (pd.DataFrame): The DataFrame containing the column with missing values.
    * col (str): The name of the column with missing values.
    """
    num_rows = df.shape[0]

    # Define the start and end indices for the regression segment
    start = 3  # Starting from the 4th entry
    end = start + 10  # Taking 10 entries for the regression

    # Extract the segment for linear regression
    segment = df[col].iloc[start:end]

    # Prepare data for linear regression
    X = np.array(range(start, end)).reshape(-1, 1)
    y = segment.values

    # Fit linear regression model
    model = LinearRegression()
    model.fit(X, y)

    # Fill first 3 NaNs
    predict_start_index = list(range(start - 3, start))
    predictions_start = model.predict(np.array(predict_start_index).reshape(-1, 1))
    for i, idx in enumerate(predict_start_index):
        df.at[idx, col] = predictions_start[i]  # Extract scalar value

    start = num_rows - 13  # Starting from the 4th entry
    end = start + 10  # Taking 10 entries for the regression
    # Extract the segment for linear regression
    segment = df[col].iloc[start:end]
    # Prepare data for linear regression
    X = np.array(range(start, end)).reshape(-1, 1)
    y = segment.values

    # Fit linear regression model
    model = LinearRegression()
    model.fit(X, y)

    # Fill last 3 NaNs
    predict_end_index = list(range(end, end + 3))
    predictions_end = model.predict(np.array(predict_end_index).reshape(-1, 1))
    for i, idx in enumerate(predict_end_index):
        if predictions_end[i] <= 0:
            predictions_end[i] = 0
        df.at[idx, col] = predictions_end[i]  # Extract scalar value


def determine_load_factor():
    """
        Calculates the estimated load factor and Fetcherr booking offset ratio for a specific flight on a given date.

        Args:
            origin (str): The origin airport code for the flight.
            destination (str): The destination airport code for the flight.
            flight_no (str): The flight number.
            departure_date (str): The departure date for the flight (format expected by date/time library used).

        Returns:
            tuple[float, float]: A tuple containing:
                * offset_ratio (float): The offset ratio for Fetcherr booking predictions, representing the
                relative adjustment needed to align with the total available units (AUs).
                * load_factor (float): The estimated load factor for the specified flight and date.
    """
    curves, df_kpis = generate_curves()
    flight_df = get_flight_df(df=df_kpis, origin=origin,
                              destination=destination,
                              flight_no=flight_no,
                              departure_date=departure_date)
    flight_df = get_flight_df_with_future_dtds(flight_df, max_dtd=366)
    flight_df = get_predicted_flight_df(curves,
                                        flight_df,
                                        hl_ftchr=13,
                                        hl_other=500,
                                        last_dtd=30)
    offset_ratio = get_offset_ratio(flight_df)
    return offset_ratio


def generate_squad_1_df():
    """
        Generates a filtered DataFrame containing specific flight data for Squad 1 calculations.

        Args:
            origin (str): The origin airport code.
            destination (str): The destination airport code.
            cabin (str): The cabin class
            is_direct (bool): Whether to filter for direct flights (True) or all flights (False).
            start_issue_date (str): The start date for filtering flight issuance dates
            departure_date (str): The departure date for the flights.

        Returns:
            pd.DataFrame: A filtered DataFrame containing relevant flight data for Squad 1 calculations.
    """
    df_sq1 = read_de_dataset(
        sq1_bucket_name="fetcherr-virgin-sandbox-data-us-raw",
        sq1_gs_prefix="data_prep/20231213_poc_experiment_fullrange_prorated/",
        files_in_gs=['data_prep/20231213_poc_experiment_fullrange_prorated/DEL_LHR.parquet'],
        local_files_location='/Users/saarteboul/Documents/Price_Calculator/uris_task/',
        sq1_file_name_template="*.parquet"
    )

    df_sq1['commodity__departure_date'] = pd.to_datetime(df_sq1['commodity__departure_date']).dt.date

    mask = (
            (df_sq1['commodity__origin'] == origin) &
            (df_sq1['commodity__destination'] == destination) &
            (df_sq1['commodity__cabin'] == cabin) &
            (df_sq1['commodity__non_stop'] == is_direct) &
            (df_sq1['flight__issue_date'] >= pd.to_datetime(start_issue_date)) &
            (df_sq1['flight__issue_date'] <= pd.to_datetime(end_issue_date)) &
            (df_sq1['commodity__departure_date'] == departure_date)
    )
    assert is_direct
    df_sq1 = df_sq1[mask]
    return df_sq1


def merge_squad_1_and_cl_df(df_cl, df_sq1):
    """
    Merges Squad 1 and Competitor Landscape (CL) data into a single DataFrame.

    Args:
        df_cl (pd.DataFrame): The Competitor Landscape DataFrame containing pricing and other data.
        df_sq1 (pd.DataFrame): The Squad 1 DataFrame containing booking information and competitor details.

    Returns:
        tuple[pd.DataFrame, list[str], list[str]]: A tuple containing:
            * df_merged (pd.DataFrame): The merged DataFrame combining data from both sources.
            * price_cols (list[str]): A list of all columns related to price in the merged DataFrame.
            * price_alternatives (list[str]): A list of price columns from CL data
    """
    commodity1_column_rename_map = {
        'observation_date': 'flight__issue_date',
        'departure_date': 'commodity__departure_date',
        'origin_commodity': 'commodity__origin',
        'destination_commodity': 'commodity__destination',
        'cabin_commodity': 'commodity__cabin'
    }
    sq1_cols_1 = ['commodity__non_stop', 'commodity__direction', 'commodity__min_stay', 'commodity__period_of_day',
                  'commodity__dtd', 'flight__departure_day_of_week']
    sq1_cols_2 = ['leaky__representative_price']
    df_cl.rename(columns=commodity1_column_rename_map, inplace=True)
    merge_keys_columns = list(commodity1_column_rename_map.values())
    df = merge_squad1_and_alternatives(
        df_sq1=df_sq1,
        df_alternatives=df_cl,
        merge_keys_columns=merge_keys_columns,
        sq1_cols_1=sq1_cols_1,
        sq1_cols_2=sq1_cols_2
    )
    price_cols = [col for col in df.columns if 'price' in col]
    price_alternatives = [col for col in price_cols if col != 'leaky__representative_price']
    return df, price_cols, price_alternatives


def apply_competitors_offset(offset_ratio):
    """
        Generates the adjusted flight prices
        Args:
            offset_ratio: the offset that was calculated according to the load factor

        Returns:
            result_Df (pd.DataFrame) containing:
                * price_adjusted_to_offset - a column with the adjusted prices.
    """
    df_sq1 = generate_squad_1_df()
    print('finished generate_squad_1_df')
    # build_competitive_landscape is heavy - takes time to execute
    df_cl = build_competitive_landscape(
        origin=origin,
        destination=destination,
        cabin=cabin,
        is_direct=is_direct,
        start_issue_date=start_issue_date,
        end_issue_date=end_issue_date,
        departure_date_shifts=departure_date_shifts
    )
    # df_cl = pd.read_csv('df_cl.csv')
    print('finished build_competitive_landscape')

    df, price_cols, price_alternatives = merge_squad_1_and_cl_df(df_cl, df_sq1)
    cabin_type = df['commodity__cabin'].unique().tolist()[0]
    optimized_params_var_name = f"optimized_params_{cabin_type}"
    optimized_params_dict = getattr(optimized_params, optimized_params_var_name, None)
    df['offset'] = calculate_price_offset_for_params(optimized_params_dict, df, return_params=False,
                                                     price_column='leaky__representative_price')
    # df['offset'] = df.apply(
    #     lambda row: calculate_price_offset_for_params(row, df, 'leaky__representative_price', return_params=False), axis=1)

    print('finished calculate_price_offset_for_params')
    # -------------------Read static table from Kyril notebook-------------------
    concluded_rank_vs_demand_table = pd.read_csv('Gradient_Desc_static_table.csv')
    current_script_dir = os.path.dirname(os.path.abspath(__file__))
    folder_path = os.path.join(current_script_dir, 'demand_tables')
    df['closest_bin_demand'] = df.apply(lambda row: find_closest_bin(row, folder_path), axis=1)
    # df['closest_bin_demand'] = df['offset'].apply(lambda x: find_closest_bin(x, concluded_rank_vs_demand_table))

    df['Demand_change_according_to_load_factor'] = max(offset_ratio, 0.0001) * df['closest_bin_demand']
    df['Demand_change_according_to_load_factor_capped'] = df.apply(lambda row: cap_value(row, folder_path), axis=1)
    # ----------- Number of steps is heavy - reduce in order to run faster ------------
    prices, mapped_offsets = get_prices_mapped_to_offsets(from_price=df.leaky__representative_price.min() * 0.8,
                                                          to_price=df.leaky__representative_price.max() * 1.2,
                                                          steps=100,
                                                          dft=df,
                                                          optimized_params=optimized_params_dict)
    print('finished get_prices_mapped_to_offsets')
    # df['closest_offset_after_adjusting_demand'] = df['Demand_change_according_to_load_factor_capped'].apply(
    #     lambda x: find_nearest_offset(x, concluded_rank_vs_demand_table))
    df['closest_offset_after_adjusting_demand'] = df.apply(
        lambda row: find_nearest_offset(row, folder_path),
        axis=1
    )
    df_with_prices = pd.concat([df, mapped_offsets], axis=1)
    result_df = find_nearest_matching_offset_df(df_with_prices.copy(),
                                                "closest_offset_after_adjusting_demand",
                                                "_offset")
    return result_df


def adjust_prices(df):
    """
        Adjusts flight prices in the DataFrame based on the 'commodity__min_stay' condition.
        Ensures that for each unique combination of 'commodity__origin', 'commodity__destination',
        'commodity__cabin', 'commodity__non_stop', 'commodity__period_of_day', 'commodity__direction',
        and 'commodity__dtd', the price for 'commodity__min_stay=0' is at least 5% higher than the price
        for 'commodity__min_stay=7'. If the condition is not met, the function adjusts the price.

        Args:
            df (pd.DataFrame): A pandas DataFrame containing flight data with the following columns:
                - 'commodity__origin'
                - 'commodity__destination'
                - 'commodity__cabin'
                - 'commodity__non_stop'
                - 'commodity__period_of_day'
                - 'commodity__direction'
                - 'commodity__dtd'
                - 'commodity__min_stay'
                - 'price_adjusted_to_offset'

        Returns:
            df (pd.DataFrame): The modified DataFrame containing:
                - 'price_adjusted_to_offset': A column with the adjusted prices.
                - 'price_adjusted_rule_min_stay': A boolean column indicating whether a price was adjusted.
        """
    # Identifying unique combinations
    unique_combinations = df[['commodity__origin', 'commodity__destination', 'commodity__cabin',
                              'commodity__non_stop', 'commodity__period_of_day',
                              'commodity__direction', 'commodity__dtd']].drop_duplicates()

    # Initialize a column to track adjustments
    df['price_adjusted_rule_min_stay'] = False

    for _, row in unique_combinations.iterrows():
        # Filter dataframe for the current combination and min_stay values
        min_stay_0 = df[(df['commodity__origin'] == row['commodity__origin']) &
                        (df['commodity__destination'] == row['commodity__destination']) &
                        (df['commodity__cabin'] == row['commodity__cabin']) &
                        (df['commodity__non_stop'] == row['commodity__non_stop']) &
                        (df['commodity__period_of_day'] == row['commodity__period_of_day']) &
                        (df['commodity__direction'] == row['commodity__direction']) &
                        (df['commodity__dtd'] == row['commodity__dtd']) &
                        (df['commodity__min_stay'] == 0)]

        min_stay_7 = df[(df['commodity__origin'] == row['commodity__origin']) &
                        (df['commodity__destination'] == row['commodity__destination']) &
                        (df['commodity__cabin'] == row['commodity__cabin']) &
                        (df['commodity__non_stop'] == row['commodity__non_stop']) &
                        (df['commodity__period_of_day'] == row['commodity__period_of_day']) &
                        (df['commodity__direction'] == row['commodity__direction']) &
                        (df['commodity__dtd'] == row['commodity__dtd']) &
                        (df['commodity__min_stay'] == 7)]

        # Check if both min_stay values exist in the current combination
        if not min_stay_0.empty and not min_stay_7.empty:
            # Calculate the minimum price for min_stay=0 to be 5% higher than min_stay=7
            required_price = min_stay_7['price_adjusted_to_offset'].max() * 1.05

            # Identify rows where adjustment is needed
            adjust_rows = min_stay_0[min_stay_0['price_adjusted_to_offset'] < required_price]

            # Adjust prices and mark as adjusted
            df.loc[adjust_rows.index, 'price_adjusted_to_offset'] = required_price
            df.loc[adjust_rows.index, 'price_adjusted_rule_min_stay'] = True

    return df


