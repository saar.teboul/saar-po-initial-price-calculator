# saar-po-initial-price-calculator


## PO initial price calculator
Giving po team an initial price for the optimisation process

## Description
Upon doing the optimization process we might encounter some problems:
* The data isn't clean
* The model isn't as good as we wish

This might cause the optimisation process to get into a divergence state

In order to keep that in line - we want it not to deviate much from the initial price.

The goal of this module is to supply PO team with a module that generates this initial price.

We shall use 2 sub-modules in order to achieve this
1. CBM (Michal) - Part which analyze the plane load factor
2. Competition analysis (Kyril) - analizing the competition 


## Usage
1. In order to calculate just the lood factor offset, go into the file determine_load_factor.py and run main
2. In order to run full capability (generate the prices column with the adjustment) , go into calculate_new_price.py anda run main
3. Kiryl Notebook - which uses the gradient decent should not be run without authorization, it can be found under Calculate_Competitive_landscape_optimal_params.ipynb
4. The output of Kiryl Notebook is the Gradient_Desc_static_table.csv and the optimized_params.py - those should not be changed
