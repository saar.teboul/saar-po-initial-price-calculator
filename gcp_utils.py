from os.path import join

from google.cloud import storage
import os
from google.cloud import bigquery
import glob

import pandas as pd


def initialize():
    # os.environ[
    #     "GOOGLE_APPLICATION_CREDENTIALS"
    # ] = "/Users/uriyerushalmi/.config/gcloud/application_default_credentials.json"
    pd.set_option("display.max_columns", None)
    pd.set_option("display.width", 2000)
    pd.set_option("display.max_rows", 2000)


def get_bigquery_client():
    project_id = "fetcherr-virgin-sandbox"
    bq_client = bigquery.Client(project=project_id)
    return bq_client


def query_as_df(bq_client, query):
    query_job = bq_client.query(query)
    answer = query_job.result().to_dataframe()
    return answer


def get_storage_client():
    project_id = "fetcherr-virgin-sandbox"
    storage_client = storage.Client(project=project_id)
    return storage_client


def list_blobs_with_prefix(bucket_name, prefix):
    storage_client = get_storage_client()
    blobs = storage_client.list_blobs(bucket_name, prefix=prefix)
    return [blob.name for blob in blobs]


def download_blob(bucket_name, source_blob_name, destination_file_name):
    storage_client = get_storage_client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(source_blob_name)
    blob.download_to_filename(destination_file_name)


def read_de_dataset(sq1_bucket_name, sq1_gs_prefix, files_in_gs, local_files_location, sq1_file_name_template):
    local_file_names = glob.glob(local_files_location + sq1_gs_prefix + sq1_file_name_template)

    for file in files_in_gs:
        file_path = join(local_files_location, file)

        if file_path not in local_file_names:
            print(f"Downloading: {file}")
            os.makedirs(os.path.dirname(file_path), exist_ok=True)
            download_blob(sq1_bucket_name, file, file_path)
            local_file_names.append(file_path)

    print(f"✅ Downloaded {len(local_file_names)} files")

    dfs = []

    for file in files_in_gs:
        print(f"Reading {file}")
        file_name = join(local_files_location, file)
        dfs.append(pd.read_parquet(file_name))

    return pd.concat(dfs)
